import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Tetanus extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/tetanus.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Tetanus is a serious disease caused by a bacterial toxin that affects
                             your nervous system, leading to painful muscle contractions, 
                             particularly of your jaw and neck muscles. Tetanus can interfere 
                             with your ability to breathe and can threaten your life. Tetanus is 
                             commonly known as "lockjaw". Tetanus is caused by a toxin made by spores 
                             of bacteria, Clostridium tetani, found in soil, dust and animal feces. 
                             When the spores enter a deep flesh wound, they grow into bacteria that 
                             can produce a powerful toxin, tetanospasmin. The toxin impairs the nerves 
                             that control your muscles (motor neurons). The toxin can cause muscle 
                             stiffness and spasms.
                            </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 3 }}>

                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/tetanus2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 370,
                                    alignSelf: "center"
                                }} />
                        </View>

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            Symptoms{"\n"}
                                Signs and symptoms of tetanus appear anytime from a few
                                 days to several weeks after tetanus bacteria enter your body 
                                 through a wound. The average incubation period is seven to 10 days.{"\n"}{"\n"}
                                Common signs and symptoms of tetanus include:{"\n"}
                                •	Spasms and stiffness in your jaw muscles.{"\n"}
                                •	Stiffness of your neck muscles.{"\n"}
                                •	Difficulty swallowing.{"\n"}
                                •	Stiffness of your abdominal muscles.{"\n"}
                                •	Painful body spasms lasting for several minutes, typically 
                                triggered by minor occurrences, such as a draft, loud noise, 
                                physical touch or light.{"\n"}{"\n"}
                                Possible other signs and symptoms include:{"\n"}
                                •	Fever{"\n"}
                                •	Sweating{"\n"}
                                •	Elevated blood pressure{"\n"}
                                •	Rapid heart rate{"\n"}{"\n"}
                                Prevention:{"\n"}
                                •	Getting vaccinated against tetanus{"\n"}
                                •	Cleaning and dressing skin wounds right away 
                                (although cleaning the wound is important, medical 
                                attention must be sought after).{"\n"}{"\n"}

                                What to do?{"\n"}
                                •	See your doctor immediately to obtain a tetanus booster shot.{"\n"}
                            </Text>
                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 5,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 330,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 710,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});