import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class BePrepared extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, display: 'flex' }}>
                <View style={{ flex: 0.40 }} >
                    <Header style={{ backgroundColor: "gray", height: 30 }}>
                        {/* <Left>

                        </Left>
                        <Right>
                            <Body>
                                <Text style={{ color: "yellow" }}>PERC</Text>
                            </Body>
                        </Right> */}
                    </Header>
                </View>
                <View style={{ flex: 2.5, flexDirection: "column", alignItems: "center" }}>
                    <View style={{ flex: 1, flexDirection: "row", alignItems: "stretch" }}>
                        <TouchableOpacity onPress={() => navigate('NaturalHazardNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/naturaldisaster.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 70,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red"}}>NATURAL DISASTER</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('BioHazardNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/biologicalhazard.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 70,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red"}}>BIOLOGICAL HAZARDS</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row", alignItems: "stretch" }}>
                        <TouchableOpacity onPress={() => navigate('TechnoHazardNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/technologicalhazard.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 95,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{fontSize: 10,textAlign:"center", color:"red"}} >TECHNOLOGICAL HAZARDS</Text>

                                </CardItem>
                            
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('FirstAidNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/firstaid.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 95,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red"}}>FIRST-AID</Text>

                                </CardItem>
                            </Card>
                            
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        marginLeft: 10,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    cardStyle: {
        height: 175,
        width: 175,
        marginLeft: 15,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});