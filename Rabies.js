import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Rabies extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/rabies.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                                Rabies is a deadly virus spread to people from the saliva of infected animals.
                                  The rabies virus is usually transmitted through a bite.
                                  Animals most likely to transmit rabies in developing countries of Africa and Southeast Asia,
                                  stray dogs are the most likely to spread rabies to people.
                                  Once a person begins showing signs and symptoms of rabies,
                                  the disease is nearly always fatal. For this reason,
                              anyone who may have a risk of contracting rabies should receive rabies vaccines for protection.
                            </Text>
                            {/* <CardItem >
                                <Body>
                                    <Text style={{ textAlign: "justify" }}>Rabies is a deadly virus spread to people from the saliva of infected animals.
                                    The rabies virus is usually transmitted through a bite. */}
                            {/* Animals most likely to transmit rabies in developing countries of Africa and Southeast Asia,
                                    stray dogs are the most likely to spread rabies to people.
                                    Once a person begins showing signs and symptoms of rabies,
                                    the disease is nearly always fatal. For this reason,
                                anyone who may have a risk of contracting rabies should receive rabies vaccines for protection. */}
                            {/* </Text>

                                </Body>

                            </CardItem> */}
                        </Card>
                    </View>
                    <View style={{ flex: 3 }}>

                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/rabies2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 370,
                                    alignSelf: "center"
                                }} />
                        </View>

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                                The first symptoms of rabies may be very similar to the flu and may last for days.
                                Later signs and symptoms may include:{"\n"}
                                •	Fever{"\n"}
                                •	Headache{"\n"}
                                •	Nausea{"\n"}
                                •	Vomiting{"\n"}
                                •	Agitation{"\n"}
                                •	Anxiety{"\n"}
                                •	Confusion{"\n"}
                                •	Hyperactivity{"\n"}
                                •	Difficulty swallowing{"\n"}
                                •	Excessive salivation{"\n"}
                                •	Fear of water (hydrophobia) because of the difficulty in swallowing{"\n"}
                                •	Hallucinations{"\n"}
                                •	Insomnia{"\n"}
                                •	Partial paralysis{"\n"}
                                {"\n"}Prevention:{"\n"}
                                •	Vaccine your dogs and cats against rabies{"\n"}
                                •	Avoid contact with wild animals and strays.{"\n"}
                                •	Keep your pets under control and away from wild animals.{"\n"}
                                {"\n"}What to do{"\n"}
                                •	Clean the bite area with soap and water for at least 10 minutes.{"\n"}
                                •	Seek professional medical help immediately. Do not wait for the 
                                symptoms to appear.{"\n"}
                                •	Bring the information about the animal if possible.{"\n"}
                                •	Receive shots of human rabies immune globulin (HRIG) and
                                 vaccine shots. The decision is usually based on the conditions of the bite, 
                                 the animal’s information, and the recommendation of the healthcare provider.{"\n"}
                            </Text>
                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 5,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 250,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 800,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});