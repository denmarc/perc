import { createStackNavigator,createSwitchNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";

import Login from './Login';
import HomeScreen from './HomeScreen';
import BePrepared from './BePrepared';
import NaturalHazard from './NaturalHazard';
import Earthquake from './Earthquake';
import BioHazard from './BioHazard';
import Flood from './Flood';
import Typhoon from './Typhoon';
import TechnoHazard from './TechnoHazard';
import FirstAid from './FirstAid';
import SplashScreen from './SplashScreen';
import Quiz from './Quiz';
import Hotline from './Hotline';
import Rabies from './Rabies';
import Register from './Register';
import Tetanus from './Tetanus';
import Fire from './Fire';
import Vehicular from './Vehicular';
import Burns from './Burns';
import CPR from './CPR';
import Sprain from './Sprain';
import Nosebleed from './Nosebleed';
import Landslide from './Landslide';
import Volcanic from './Volcanic';
import Malaria from './Malaria';
import Dengue from './Dengue';
import HIV from './HIV';
import Meningitis from './Meningitis';
import Leptos from './Leptos';
import Explosion from './Explosion';
import Cuts from './Cuts';
import Fractures from './Fractures';
import Floorplan from './Floorplan';
import Community from './Community';
import Splinter from './Splinter';

AppStack = createStackNavigator( // Navigator to all Screens
    {
        SplashScreenNav: {
            screen: SplashScreen,
            navigationOptions: {
                header: null
            }
        },
        LoginNav: {
            screen: Login,
            navigationOptions: {
                header: null
            }
        },
        HomeScreenNav: {
            screen: HomeScreen,
            navigationOptions: {
                header: null
            }
        },
        BePreparedNav: {
            screen: BePrepared,
            navigationOptions: {
                    title: 'Be Prepared'
            }
        },
        NaturalHazardNav: {
            screen: NaturalHazard,
            navigationOptions: {
                title: 'Natural Hazards'
            }
        },
        EarthquakeNav: {
            screen: Earthquake,
            navigationOptions: {
                title: 'Earthquake'      
            }
        },
        BioHazardNav: {
            screen: BioHazard,
            navigationOptions: {
                title: 'Biological Hazards'
            }
        },
        FloodNav: {
            screen: Flood,
            navigationOptions: {
                title: 'Flood'
            }
        },
        TyphoonNav: {
            screen: Typhoon,
            navigationOptions: {
                title: 'Typhoon'
            }
        },
        TechnoHazardNav: {
            screen: TechnoHazard,
            navigationOptions: {
                title: 'Technological Hazard'
            }
        },
        FirstAidNav: {
            screen: FirstAid,
            navigationOptions: {
                title: 'First-Aid'
            }
        },
        QuizNav: {
            screen: Quiz,
            navigationOptions: {
                title: 'Quiz Time'
            }
        },
       HotlineNav: {
            screen:  Hotline,
            navigationOptions: {
                title: 'Emergency Hotline'
            }
        },
        RabiesNav: {
            screen:  Rabies,
            navigationOptions: {
                title: 'Rabies'
            }
        },
        RegisterNav: {
            screen:  Register,
            navigationOptions: {
                title: 'Register'
            }
        },
        TetanusNav: {
            screen:  Tetanus,
            navigationOptions: {
                title: 'Tetanus'
            }
        },
        FireNav: {
            screen:  Fire,
            navigationOptions: {
                title: 'Fire'
            }
        },
        VehicularNav: {
            screen:  Vehicular,
            navigationOptions: {
                title: 'Vehicular'
            }
        },
        BurnsNav: {
            screen:  Burns,
            navigationOptions: {
                title: 'Burns'
            }
        },
        CPRNav: {
            screen:  CPR,
            navigationOptions: {
                title: 'CPR'
            }
        },
        SprainNav: {
            screen:  Sprain,
            navigationOptions: {
                title: 'Sprain'
            }
        },
        NosebleedNav: {
            screen:  Nosebleed,
            navigationOptions: {
                title: 'Nosebleed'
            }
        },
        LandslideNav: {
            screen:  Landslide,
            navigationOptions: {
                title: 'Landslide'
            }
        },
       VolcanicNav: {
            screen:  Volcanic,
            navigationOptions: {
                title: 'Volcanic Eruption'
            }
        },
        MalariaNav: {
             screen:  Malaria,
             navigationOptions: {
                 title: 'Malaria'
             }
         },
         DengueNav: {
              screen:  Dengue,
              navigationOptions: {
                  title: 'Dengue'
              }
          },
        HIVNav: {
            screen:  HIV,
            navigationOptions: {
                title: 'HIV'
            }
        },
        MeningitisNav: {
            screen:  Meningitis,
            navigationOptions: {
                title: 'Meningitis'
            }
        },
        LeptosNav: {
                screen:  Leptos,
                navigationOptions: {
                    title: 'Leptospirosis'
                }
            },
        ExplosionNav: {
            screen:  Explosion,
            navigationOptions: {
                title: 'Explosion'
            }
        },
        CutsNav: {
            screen:  Cuts,
            navigationOptions: {
                title: 'Cuts and Scrapes'
            }
        },
        FracturesNav: {
            screen:  Fractures,
            navigationOptions: {
                title: 'Fractures'
            }
        },
        FloorplanNav: {
            screen:  Floorplan,
            navigationOptions: {
                title: 'Floorplan'
            }
        },
        CommunityNav: {
            screen:  Community,
            navigationOptions: {
                title: 'Community'
            }
        },
        SplinterNav: {
            screen:  Splinter,
            navigationOptions: {
                title: 'Splinter'
            }
        }
            
        
    }
)

export default createAppContainer(AppStack);
