import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity,
    Linking } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Community extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 2, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyleLogo}>
                            <CardItem >
                                <Image source={require('./assets/image/community.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    {/* <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            https://www.pdrf.org/{"\n"}{"\n"}
                            https://thediplomat.com/2017/11/preparing-for-disaster-in-the-philippines/{"\n"}

                                         </Text>

                        </Card>
                    </View> */}

                    <TouchableOpacity onPress={()=>{Linking.openURL('https://www.pdrf.org/')}}  activeOpacity={0.7}> 
                        <Card style={styles.cardStyle}>    
                            <CardItem style={styles.cardStyle}>
                                <Left>
                                    <Body>
                                        <Text style={styles.cardText}>Philippine Disaster Resilience Foundation</Text>
                                    </Body>
                                    <Thumbnail source={require('./assets/image/pdrf.png')} resizeMode='contain' />
                                </Left>
                            </CardItem> 
                        </Card>
                     </TouchableOpacity>
                     <TouchableOpacity onPress={()=>{Linking.openURL('https://thediplomat.com/2017/11/preparing-for-disaster-in-the-philippines/')}}  activeOpacity={0.7}> 
                        <Card style={styles.cardStyle}>    
                            <CardItem style={styles.cardStyle}>
                                <Left>
                                    <Body>
                                        <Text style={styles.cardText}>Preparing for Disaster in the Philippines</Text>
                                    </Body>
                                    <Thumbnail source={require('./assets/image/diplomat.png')} resizeMode='contain' />
                                </Left>
                            </CardItem> 
                        </Card>
                     </TouchableOpacity>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },

    cardStyleMain: {
        height: 50,
        width: 300,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 5,

    },
    cardStyleLogo: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle: {
        height: 60,
        width: 300,
        marginBottom: 10,
        justifyContent: 'center',
        alignSelf: "center",
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 9,
        // },
        // shadowOpacity: 0.50,
        // shadowRadius: 11.65,
        // elevation: 20,
    },
    cardStyle2: {
        height: 100,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1150,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});