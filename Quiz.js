import React, { Component } from 'react';
import { ScrollView, View, Text,StyleSheet, Button } from 'react-native';
import RadioForm, { 
        RadioButton, 
        RadioButtonInput, 
        RadioButtonLabel
    } from 'react-native-simple-radio-button';

var quiz1 = [
    { label: 'Typhoon', value: 1 },
    { label: 'Earthquake', value: 2 },
    { label: 'Flood', value: 3 },
    { label: 'Fire', value: 4 }
];
var quiz2 = [
    { label: 'Expect aftershocks', value: 1 },
    { label: 'Make up plan', value: 2 },
    { label: 'Learn firstaid', value: 3 },
    { label: 'Duck, Cover and Hold', value: 4 }
];
var quiz3 = [
    { label: 'Flood', value: 1 },
    { label: 'Burn', value: 2 },
    { label: 'Tetanus', value: 3 },
    { label: 'Earthquake', value: 4 }
];
var quiz4 = [
    { label: 'Drinking water', value: 1 },
    { label: 'Flashlights', value: 2 },
    { label: 'Car', value: 3 },
    { label: 'Medications and first aid supplies', value: 4 }
];
var quiz5 = [
    { label: 'Burn', value: 1 },
    { label: 'Fire', value: 2 },
    { label: 'Vehicular Accident', value: 3 },
    { label: 'Rabies', value: 4 }
];
var quiz6 = [
    { label: 'Typhoon', value: 1 },
    { label: 'Tetanus', value: 2 },
    { label: 'Nosebleed', value: 3 },
    { label: 'Fire', value: 4 }
];
var quiz7 = [
    { label: 'Tetanus', value: 1 },
    { label: 'Sprain/Strain', value: 2 },
    { label: 'Rabies', value: 3 },
    { label: 'Burn', value: 4 }
];
var quiz8 = [
    { label: 'Headache', value: 1 },
    { label: 'Nausea', value: 2 },
    { label: 'Vomiting', value: 3 },
    { label: 'HIV', value: 4 }
];
var quiz9 = [
    { label: 'Tetanus', value: 1 },
    { label: 'Burn', value: 2 },
    { label: 'Nosebleed', value: 3 },
    { label: 'Spraid/Strain', value: 4 }
];
var quiz10 = [
    { label: 'Drunk Driving', value: 1 },
    { label: 'Speeding', value: 2 },
    { label: 'Reckless Driving', value: 3 },
    { label: 'Safety Driving', value: 4 }
];
var quiz11 = [
    { label: 'Second-degree burns', value: 1 },
    { label: 'Third-degree burns', value: 2 },
    { label: 'First-degree burns', value: 3 },
    { label: 'Fourth-degree burns', value: 4 }
];
var quiz12 = [
    { label: 'Common Perspiratory Respiratory', value: 1 },
    { label: 'Cardiopulmonary Resuscitation', value: 2 },
    { label: 'CardioPulmoniac Respiration', value: 3 },
    { label: 'Commonpulmonary Respiration', value: 4 }
];

export default class SandboxScreen extends Component {
    constructor(props){
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    state = {
        quiz1: 1,
        quiz2: 1,
        quiz3: 1,
        quiz4: 1,
        quiz5: 1,
        quiz6: 1,
        quiz7: 1,
        quiz8: 1,
        quiz9: 1,
        quiz10: 1,
        quiz11: 1,
        quiz12: 1
    }

    handleSubmit(event){
        event.preventDefault();
        alert('Your score is ' + this.getScore() + ' out of 12 items.' );
        console.log(JSON.stringify(this.state));
    }

    getScore(){
        return score = (this.state.quiz1 === 2 ? 1 : 0) + 
                       (this.state.quiz2 === 4 ? 1 : 0) + 
                       (this.state.quiz3 === 1 ? 1 : 0) +
                       (this.state.quiz4 === 3 ? 1 : 0) +
                       (this.state.quiz5 === 2 ? 1 : 0) +
                       (this.state.quiz6 === 1 ? 1 : 0) +
                       (this.state.quiz7 === 3 ? 1 : 0) +
                       (this.state.quiz8 === 4 ? 1 : 0) +
                       (this.state.quiz9 === 1 ? 1 : 0) +
                       (this.state.quiz10 === 4 ? 1 : 0) +
                       (this.state.quiz11 === 3 ? 1 : 0) +
                       (this.state.quiz12 === 2 ? 1 : 0);
    }

    render(){
        return(
            <ScrollView>
                {/* quiz 1 */}
                <Text style={styles.question}>1. What is the shaking of the surface of the Earth?</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz1}
                initial={0}
                onPress={(value) => {this.setState({quiz1:value})}}
                />
                <View style={styles.separator} />

                {/* quiz 2 */}
                <Text style={styles.question}>2. What to do during an Earthquake?</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz2}
                initial={0}
                onPress={(value) => {this.setState({quiz2:value})}}
                />
                <View style={styles.separator} />

                {/* quiz 3 */}
                <Text style={styles.question}>3.What is an overflow of water that submerges land that is usually dry?</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz3}
                initial={0}
                onPress={(value) => {this.setState({quiz3:value})}}
                />
                <View style={styles.separator} />        

                {/* quiz 4 */}
                <Text style={styles.question}>4. Assemble disaster supplies except:</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz4}
                initial={0}
                onPress={(value) => {this.setState({quiz4:value})}}
                />
                <View style={styles.separator} />

                {/* quiz 5 */}
                <Text style={styles.question}>5. What is the rapid oxidation of a material in the exothermic chemical process of combustion, releasing heat, light and various reaction products?</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz5}
                initial={0}
                onPress={(value) => {this.setState({quiz5:value})}}
                />
                <View style={styles.separator} />

                {/* quiz 6 */}
                <Text style={styles.question}>6. What is an intense circular storm that originates over warm tropical oceans?</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz6}
                initial={0}
                onPress={(value) => {this.setState({quiz6:value})}}
                />
                <View style={styles.separator} />                

                {/* quiz 7 */}
                <Text style={styles.question}>7. What is a deadly virus spread to people from the saliva of infected animals?</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz7}
                initial={0}
                onPress={(value) => {this.setState({quiz7:value})}}
                />
                <View style={styles.separator} />

                {/* quiz 8 */}
                <Text style={styles.question}>8. Later signs and symtomps of Rabies except:</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz8}
                initial={0}
                onPress={(value) => {this.setState({quiz8:value})}}
                />
                <View style={styles.separator} />

                {/* quiz 9 */}
                <Text style={styles.question}>9. What is a series disease caused by a bacterial toxin that affects your nervous system, leading to painful muscle contractions, particularly of your jaw and neck muscles?</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz9}
                initial={0}
                onPress={(value) => {this.setState({quiz9:value})}}
                />
                <View style={styles.separator} />                

                {/* quiz 10 */}
                <Text style={styles.question}>10. Causes of Vehicular Accident except:</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz10}
                initial={0}
                onPress={(value) => {this.setState({quiz10:value})}}
                />
                <View style={styles.separator} />

                {/* quiz 11 */}
                <Text style={styles.question}>11. What is the degree of burns that can cause minimal skin damage?</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz11}
                initial={0}
                onPress={(value) => {this.setState({quiz11:value})}}
                />
                <View style={styles.separator} />

                {/* quiz 12 */}
                <Text style={styles.question}>12. What is the meaning of CPR?</Text>
                <RadioForm
                style={styles.radioForm}
                radio_props={quiz12}
                initial={0}
                onPress={(value) => {this.setState({quiz12:value})}}
                />
                <View style={styles.separator} />                

                {/* submit */}
                <View style={[{ width: "90%", margin: 10, backgroundColor: "red" }]}>
                    <Button
                        onPress={this.handleSubmit}
                        title="S U B M I T"
                        color="#2ca343"
                    />
                </View>
            </ScrollView>
    
            );
    }
}

const styles = StyleSheet.create({
    question: {
        fontSize: 15,
        marginTop: 10,
        marginLeft: 10,
        
    },
    radioForm: {
        marginTop: 15,
        marginLeft: 30,
    },
    separator: {
        marginTop: 20,
        backgroundColor: '#eaeaea',
        height: 1,
      },
});


console.disableYellowBox = true;