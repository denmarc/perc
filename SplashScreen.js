import React from 'react';
import { View, Text, Image } from 'react-native';

class SplashScreen extends React.Component {
    performTimeConsumingTask = async () => {
        return new Promise((resolve) =>
            setTimeout(
                () => { resolve('result') },
                3000
            )
        )
    }

    async componentDidMount() {
        // Preload data from an external API
        // Preload data using AsyncStorage
        const data = await this.performTimeConsumingTask();

        if (data !== null) {
            // this.props.navigation.navigate('LoginNav');
            this.props.navigation.navigate('HomeScreenNav');
        }
    }

    render() {
        return (
            <View style={styles.viewStyles}>
                {/* <Text style={styles.textStyles}>
          HELLO RYNAN
        </Text> */}

                <Image
                    resizeMode='stretch'
                    style={{
                        width: 280,
                        height: 150,
                        alignSelf: 'center',

                    }}
                    source={require('./assets/image/logo.png')}
                />
            </View>
        );
    }
}

const styles = {
    viewStyles: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    textStyles: {
        color: 'red',
        fontSize: 40,
        fontWeight: 'bold'
    }
}

export default SplashScreen;
