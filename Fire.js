import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Fire extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>

                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/fire.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Fire is the rapid oxidation of a material in the exothermic chemical process
                             of combustion, releasing heat, light, and various reaction products. 
                             Fires start when a flammable or a combustible material, in combination 
                             with a sufficient quantity of an oxidizer such as oxygen gas or another 
                             oxygen-rich compound (though non-oxygen oxidizers exist), is exposed to 
                             a source of heat or ambient temperature above the flash point for the
                              fuel/oxidizer mix, and is able to sustain a rate of rapid oxidation that 
                              produces a chain reaction.
                            </Text>
                        </Card>
                    </View>
                    <View style={{ flex: 3 }}>

                        <View style={styles.imageStyle2}>
                            <Image source={require('./assets/image/fire2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 370,
                                    alignSelf: "center"
                                }} />
                        </View>

                        <View style={styles.imageStyle2}>
                            <Image source={require('./assets/image/fires.jpg')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 370,
                                    alignSelf: "center"
                                }} />
                        </View>

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            If a Fire starts:{"\n"}
                            •	Know how to safely operate a fire extinguisher.{"\n"}
                            •	Remember to GET OUT, STAY OUT and call your local emergency phone number.{"\n"}
                            •	Yell "Fire!" several times and go outside right away. {"\n"}
                            If you live in a building with elevators, use the stairs. 
                            Leave all your things where they are and save yourself.{"\n"}
                            •	If closed doors or handles are warm or smoke blocks your primary escape route,
                             use your second way out. Never open doors that are warm to the touch.{"\n"}
                            •	If you must escape through smoke, get low and go under the smoke to your exit.
                             Close doors behind you.{"\n"}
                            •	If smoke, heat or flames block your exit routes, stay in the room with doors 
                            closed. Place a wet towel under the door and call the fire department or your
                             local emergency hotline. Open a window and wave a brightly colored cloth or 
                             flashlight to signal for help.{"\n"}
                            •	Once you are outside, go to your meeting place and then send one person to
                             call the fire department. If you cannot get to your meeting place, follow your
                              family emergency communication plan.{"\n"}{"\n"}
                            Note:{"\n"}
                            If your clothes catch on fire:{"\n"}
                            •	Stop what you’re doing.{"\n"}
                            •	Drop to the ground and cover your face if you can.{"\n"}
                            •	Roll over and over or back and forth until the flames go out.
                             Running will only make the fire burn faster.{"\n"}
                            Once the flames are out, cool the burned skin with water for three to five minutes.
                             Call for medical attention.
                            {"\n"}
                            </Text>
                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 5,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 250,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 800,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});