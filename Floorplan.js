import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Floorplan extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>

                        </Header>
                    </View>

                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            STI Floor Plan
                                         </Text>

                        </Card>
                    </View>
 
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/1st.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/2nd.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/3rd.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>
                    </View>


                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 50,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1150,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});