import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Leptos extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/leptos.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Leptospirosis is a bacterial disease that affects humans and animals. 
                            It is caused by bacteria of the genus Leptospira. In humans,
                             it can cause a wide range of symptoms, some of which may be mistaken for other diseases. 
                             Some infected persons, however, may have no symptoms at all.
                                Without treatment, Leptospirosis can lead to kidney damage, 
                                meningitis (inflammation of the membrane around the brain and spinal cord), 
                                liver failure, respiratory distress, and even death.

                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/leptospirosis.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

            

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            Leptospirosis can cause a wide range of symptoms, including:{"\n"}{"\n"}
                            •	High fever{"\n"}
                            •	Headache{"\n"}
                            •	Chills{"\n"}
                            •	Muscle aches{"\n"}
                            •	Vomiting{"\n"}
                            •	Jaundice (yellow skin and eyes){"\n"}
                            •	Red eyes{"\n"}
                            •	Abdominal pain{"\n"}
                            •	Diarrhea{"\n"}
                            •	Rash{"\n"}{"\n"}
                            Treatment{"\n"}{"\n"}
                            Leptospirosis is treated with antibiotics, such as doxycycline or penicillin,
                             which should be given early in the course of the disease.{"\n"}
                            Intravenous antibiotics may be required for persons with more severe symptoms.
                             Persons with symptoms suggestive of leptospirosis should contact a health care provider.{"\n"}{"\n"}
                            Prevention{"\n"}
                            The risk of acquiring leptospirosis can be greatly reduced by not swimming or wading in water 
                            that might be contaminated with animal urine, or eliminating contact with 
                            potentially infected animals.{"\n"}
                            Protective clothing or footwear should be worn by those exposed to contaminated water or
                             soil because of their job or recreational activities.{"\n"}


                           
                            </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 250,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 700,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});