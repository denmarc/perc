import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Fractures extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/fracture.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            A broken bone or bone fracture occurs when a force exerted against a bone is stronger 
                            than the bone can bear. This disturbs the structure and strength of the bone, and 
                            leads to pain, loss of function and sometimes bleeding and injury around the site
                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/fractures.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

            

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            Causes of bone fractures{"\n"}
                            Causes of bone fractures can include:{"\n"}{"\n"}
                            •	Traumatic incidents such as sporting injuries, vehicle accidents and falls{"\n"}
                            •	Conditions such as osteoporosis and some types of cancer that cause bones to
                            fracture more easily, meaning even minor trauma and falls can become serious.{"\n"}{"\n"}
                            Symptoms of bone fractures{"\n"}
                            Fractures are different from other injuries to the skeleton such as dislocations, 
                            although in some cases it can be hard to tell them apart. Sometimes, a person 
                            may have more than one type of injury. If in doubt, treat the injury as if it is a fracture.{"\n"}{"\n"}

                            The symptoms of a fracture depend on the particular bone and the severity of the injury, but may include:{"\n"}
                            •	Pain{"\n"}
                            •	Swelling{"\n"}
                            •	Bruising{"\n"}
                            •	Deformity{"\n"}
                            •	Inability to use the limb.{"\n"}{"\n"}
                            If you suspect a bone fracture, you should:{"\n"}{"\n"}
                            •	Keep the person still – do not move them unless there is an immediate danger,
                             especially if you suspect fracture of the skull, spine, ribs, pelvis or upper leg{"\n"}
                            •	Attend to any bleeding wounds first. Stop the bleeding by pressing firmly 
                            on the site with a clean dressing. If a bone is protruding, apply pressure around the edges of the wound{"\n"}
                            •	If bleeding is controlled, keep the wound covered with a clean dressing{"\n"}
                            •	Never try to straighten broken bones{"\n"}
                            •	For a limb fracture, provide support and comfort such as a pillow under the lower leg or forearm. 
                            However, do not cause further pain or unnecessary movement of the broken bone{"\n"}
                            •	Apply a splint to support the limb. Splints do not have to be professionally manufactured. 
                            Items like wooden boards and folded magazines can work for some fractures. 
                            You should immobilize the limb above and below the fracture{"\n"}
                            •	Use a sling to support an arm or collarbone fracture{"\n"}
                            •	Raise the fractured area if possible and apply a cold pack to reduce swelling and pain{"\n"}
                            •	Stop the person from eating or drinking anything until they are seen by a doctor, 
                            in case they will need surgery{"\n"}

                            </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 150,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1150,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});