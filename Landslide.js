import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Landslide extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/landslide.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Refers to several forms of mass wasting that include a
                             wide range of ground movements, such as rockfalls,
                              deep-seated slope failures, mudflows, and debris flows.
                               Landslides occur in a variety of environments, 
                               characterized by either steep or gentle slope gradients,
                                from mountain ranges to coastal cliffs or even underwater,
                                 in which case they are called submarine landslides.
                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/landslides.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

                       

                        {/* <View style={styles.imageStyle2}>
                            <Image source={require('./assets/image/afterearthquake.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

                        <View style={styles.imageStyle2}>
                            <Image source={require('./assets/image/earthquakepeople.jpg')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View> */}


                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            What To Do Before a Landslide{"\n"}
                            •	Do not build near steep slopes, close to mountain edges, 
                            near drainage ways, or natural erosion valleys.{"\n"}
                            •	Contact local officials, state geological surveys or
                             departments of natural resources, and university departments of geology. 
                             Landslides occur where they have before, and in identifiable hazard locations.
                              Ask for information on landslides in your area, specific information on areas vulnerable 
                              to landslides, and request a professional referral for a very detailed site analysis of 
                              your property, and corrective measures you can take, if necessary.{"\n"}
                            •	Watch the patterns of storm-water drainage near your home, 
                            and note the places where runoff water converges, increasing flow in channels.
                             These are areas to avoid during a storm.{"\n"}
                            •	Learn about the emergency-response and evacuation plans for your area. 
                            Develop your own emergency plan for your family or business.{"\n"}{"\n"}
                            What To Do During a Landslide{"\n"}
                            •	Stay alert and awake. Many debris-flow fatalities occur when people are 
                            sleeping. Listen to a NOAA Weather Radio or portable, battery-powered radio or
                             television for warnings of intense rainfall. Be aware that intense, short bursts of rain
                              may be particularly dangerous, especially after longer periods of heavy rainfall and 
                              damp weather.{"\n"}
                            •	If you are in areas susceptible to landslides and debris flows, consider 
                            leaving if it is safe to do so. Remember that driving during an intense storm can be hazardous. 
                            If you remain at home, move to a second story if possible. Staying out of the path of a 
                            landslide or
                             debris flow saves lives.{"\n"}
                            •	Listen for any unusual sounds that might indicate moving debris, such as trees cracking or
                             boulders knocking together.{"\n"}
                            •	If you are near a stream or channel, be alert for any sudden increase or 
                            decrease in water flow
                             and for a change from clear to muddy water. Such changes may indicate landslide 
                             activity upstream, 
                             so be prepared to move quickly. Don't delay! Save yourself, not your belongings.{"\n"}
                            •	Be aware that strong shaking from earthquakes can induce or intensify the effects of 
                            landslides.{"\n"}{"\n"}
                            What To Do After a Landslide{"\n"}
                            •	Stay away from the slide area. There may be danger of additional slides.{"\n"}
                            •	Listen to local radio or television stations for the latest emergency information.{"\n"}
                            •	Watch for flooding, which may occur after a landslide or debris flow. Floods sometimes follow 
                            landslides and debris flows because they may both be started by the same event.{"\n"}
                            •	Check for injured and trapped persons near the slide, without entering the direct slide area. 
                            Direct rescuers to their locations{"\n"}

                            </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 200,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1280,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});