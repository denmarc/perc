import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class HIV extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/hiv.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            HIV (human immunodeficiency virus) is a virus that attacks cells that help the body
                             fight infection, making a person more vulnerable to other infections and diseases.
                              It is spread by contact with certain bodily fluids of a person with HIV, 
                              most commonly during unprotected sex (sex without a condom or HIV medicine to 
                              prevent or treat HIV), or through sharing injection drug equipment.
                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/hivs.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

            

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            What Are the Symptoms of HIV?  {"\n"}  {"\n"}
                            There are several symptoms of HIV. Not everyone will have the same symptoms. 
                            It depends on the person and what stage of the disease they are in.  {"\n"}
                            Below are the three stages of HIV and some of the symptoms people may experience.  {"\n"}
                            Stage 1: Acute HIV Infection  {"\n"}
                            Within 2 to 4 weeks after infection with HIV, about two-thirds of people will have a 
                            flu-like illness. This is the body’s natural response to HIV infection.   {"\n"}  {"\n"}
                            Flu-like symptoms can include:  {"\n"}
                            •	Fever  {"\n"}
                            •	Chills  {"\n"}
                            •	Rash  {"\n"}
                            •	Night sweats  {"\n"}
                            •	Muscle aches  {"\n"}
                            •	Sore throat  {"\n"}
                            •	Fatigue  {"\n"}
                            •	Swollen lymph nodes  {"\n"}
                            •	Mouth ulcers  {"\n"}  {"\n"}
                            These symptoms can last anywhere from a few days to several weeks. But some people 
                            do not have any symptoms at all during this early stage of HIV.  {"\n"}
                            Stage 2: Clinical Latency  {"\n"}
                            In this stage, the virus still multiplies, but at very low levels. People in this stage
                             may not feel sick or have any symptoms. This stage is also called chronic HIV infection.
                            Without HIV treatment, people can stay in this stage for 10 or 15 years, but some move 
                            through this stage faster.  {"\n"}
                            If you take HIV treatment every day, exactly as prescribed and get and keep an 
                            undetectable viral load, you can protect your health and prevent transmission to others.
                             But if your viral load is detectable, you can transmit HIV during this stage, even
                              when you have no symptoms. It’s important to see your health care provider regularly
                               to get your level checked.  {"\n"}
                           
                            </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 210,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 890,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});