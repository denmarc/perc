import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Explosion extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/explosion.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Explosive devices can be highly portable, using vehicles and humans as a means of transport. 
                            They are easily detonated from remote locations or by suicide bombers. There are steps you can 
                            take to prepare for theunexpected.
                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/explosions.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

            

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            Before an Explosion{"\n"}{"\n"}
                            The following are things you can do to protect yourself, your family and
                             your property in the event of an explosion.{"\n"}
                            •	Build an Emergency Supply Kit{"\n"}
                            •	Make a Family Emergency Plan{"\n"}
                            •	Learn what to do in case of bomb threats or receiving suspicious packages and letters{"\n"}{"\n"}
                            Bomb Threats{"\n"}{"\n"}
                            If you receive a telephoned bomb threat:{"\n"}
                            •	Get as much information from the caller as possible. Try to ask the following questions:{"\n"}
                            o	When is the bomb going to explode?{"\n"}
                            o	Where is it right now?{"\n"}
                            o	What does it look like?{"\n"}
                            o	What kind of bomb is it?{"\n"}
                            o	What will cause it to explode?{"\n"}
                            o	Did you place the bomb?{"\n"}
                            •	Keep the caller on the line and record everything that is said.{"\n"}
                            •	Notify the police and building management immediately.{"\n"}{"\n"}
                            Take these additional steps against possible biological and chemical agents:{"\n"}
                            •	Never sniff or smell suspicious mail.{"\n"}
                            •	Place suspicious envelopes or packages in a plastic bag or some other type of container 
                            to prevent leakage of contents.{"\n"}
                            •	Leave the room and close the door or section off the area to prevent others from entering.{"\n"}
                            •	Wash your hands with soap and water to prevent spreading any powder to your face.{"\n"}
                            •	If you are at work, report the incident to your building security official or an available supervisor,
                             who should notify police and other authorities without delay.{"\n"}
                            •	List all people who were in the room or area when this suspicious letter or package was recognized.
                             Give a copy of this list to both the local public health authorities and law enforcement officials
                              for follow-up investigations and advice.{"\n"}
                            •	If you are at home, report the incident to local police.{"\n"}{"\n"}
                            During an Explosion{"\n"}
                            •	Get under a sturdy table or desk if things are falling around you. When they stop falling, 
                            leave quickly, watching for obviously weakened floors and stairways.{"\n"}
                            •	Do not use elevators.{"\n"}
                            •	Stay low if there is smoke. Do not stop to retrieve personal possessions or make phone calls.{"\n"}
                            •	Check for fire and other hazards.{"\n"}
                            •	Once you are out, do not stand in front of windows, glass doors or other potentially hazardous areas.{"\n"}
                            •	If you are trapped in debris, use a flashlight, whistle or tap on pipes to signal your location to rescuers.{"\n"}
                            •	Shout only as a last resort to avoid inhaling dangerous dust.{"\n"}
                            •	Cover your nose and mouth with anything you have on hand.{"\n"}{"\n"}
                            After an Explosion{"\n"}
                            •	There may be significant numbers of casualties or damage to buildings and infrastructure.{"\n"}
                            •	Heavy law enforcement involvement at local, state and federal levels.{"\n"}
                            •	Health and mental health resources in the affected communities can be strained to their limits, 
                            maybe even overwhelmed.{"\n"}
                            •	Extensive media coverage, strong public fear and international implications and consequences.{"\n"}
                            •	Workplaces and schools may be closed, and there may be restrictions on domestic and international travel.{"\n"}
                            •	You and your family or household may have to evacuate an area, avoiding roads blocked for your safety.{"\n"}
                            •	Clean-up may take many months.{"\n"}

                   
                            </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 140,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1730,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});