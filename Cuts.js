import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Cuts extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/cut.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Cuts/Scrapes
                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/cuts.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

            

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            These guidelines can help you care for minor cuts and scrapes:{"\n"}{"\n"}
                            1.	Wash your hands. This helps avoid infection.{"\n"}
                            2.	Stop the bleeding. Minor cuts and scrapes usually stop bleeding on their own. 
                            If needed, apply gentle pressure with a clean bandage or cloth and elevate the wound 
                            until bleeding stops.{"\n"}
                            3.	Clean the wound. Rinse the wound with water. Keeping the wound under running tap 
                            water will reduce the risk of infection. Wash around the wound with soap. But don't get 
                            soap in the wound. And don't use hydrogen peroxide or iodine, which can be irritating. 
                            Remove any dirt or debris with a tweezers cleaned with alcohol. See a doctor if you 
                            can't remove all debris.{"\n"}
                            4.	Apply an antibiotic or petroleum jelly. Apply a thin layer of an antibiotic ointment 
                            or petroleum jelly to keep the surface moist and help prevent scarring. Certain ingredients 
                            in some ointments can cause a mild rash in some people. If a rash appears, stop using the
                             ointment.{"\n"}
                            5.	Cover the wound. Apply a bandage, rolled gauze or gauze held in place with paper tape.
                            Covering the wound keeps it clean. If the injury is just a minor scrape or scratch, 
                            leave it uncovered.{"\n"}
                            6.	Change the dressing. Do this at least once a day or whenever the bandage becomes wet or
                             dirty.{"\n"}
                            7.	Get a tetanus shot. Get a tetanus shot if you haven't had one in the past five years and 
                            the wound is deep or dirty.{"\n"}
                            8.	Watch for signs of infection. See a doctor if you see signs of infection on the skin or 
                            near the wound, such as redness, increasing pain, drainage, warmth or swelling.{"\n"}

                           
                            </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 50,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 840,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});