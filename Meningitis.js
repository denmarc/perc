import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Meningitis extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/meningit.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Meningitis is an inflammation of the membranes (meninges) surrounding your brain and spinal cord.
                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/meningitis.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

            

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            Symptoms{"\n"}{"\n"}
                            Early meningitis symptoms may mimic the flu (influenza). {"\n"}
                            Symptoms may develop over several hours or over a few days.{"\n"}
                            Possible signs and symptoms in anyone older than the age of 2 include:{"\n"}
                            •	Sudden high fever{"\n"}
                            •	Stiff neck{"\n"}
                            •	Severe headache that seems different than normal{"\n"}
                            •	Headache with nausea or vomiting{"\n"}
                            •	Confusion or difficulty concentrating{"\n"}
                            •	Seizures{"\n"}
                            •	Sleepiness or difficulty waking{"\n"}
                            •	Sensitivity to light{"\n"}
                            •	No appetite or thirst{"\n"}
                            •	Skin rash (sometimes, such as in meningococcal meningitis){"\n"}{"\n"}
                            Prevention{"\n"}{"\n"}
                            Common bacteria or viruses that can cause meningitis can spread through coughing, 
                            sneezing, kissing, or sharing eating utensils, a toothbrush or a cigarette.{"\n"}
                            These steps can help prevent meningitis:{"\n"}
                            •	Wash your hands. Careful hand-washing helps prevent the spread of germs. 
                            Teach children to wash their hands often, especially before eating and after
                             using the toilet, spending time in a crowded public place or petting animals. 
                             Show them how to vigorously and thoroughly wash and rinse their hands.{"\n"}
                            •	Practice good hygiene. Don't share drinks, foods, straws, eating utensils, 
                            lip balms or toothbrushes with anyone else. Teach children and teens to avoid 
                            sharing these items too.{"\n"}
                            •	Stay healthy. Maintain your immune system by getting enough rest, exercising 
                            regularly, and eating a healthy diet with plenty of fresh fruits, vegetables and 
                            whole grains.{"\n"}
                            •	Cover your mouth. When you need to cough or sneeze, be sure to cover your mouth 
                            and nose.{"\n"}
                            •	If you're pregnant, take care with food. Reduce your risk of listeriosis by cooking 
                            meat, including hot dogs and deli meat, to 165 F (74 C). Avoid cheeses made from
                             unpasteurized milk. Choose cheeses that are clearly labeled as being made with 
                             pasteurized milk.{"\n"}

                               
                           
                            </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 100,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1000,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});