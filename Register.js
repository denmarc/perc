import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput} from 'react-native';
import {
    Container, Header, Title, Content,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, DatePicker, Picker
} from 'native-base';
import moment from "moment";


export default class Register extends Component {
    state = {
        language: '',
        birthdate: '',
        firstname: '',
        lastname: '',
        gender: 'male',
        username: '',
        password: '',
        admin: 0
    }
    render() {
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={ styles.container}>
                        <Text>Firstname</Text>
                        <Item rounded >
                            <Input placeholder='Enter Firstname'
                                value={this.state.firstname}
                                onChangeText={firstname => this.setState({ firstname })}
                            />
                        </Item>
                        <Text>Lastname</Text>
                        <Item rounded  >
                            <Input placeholder='Enter Lastname'
                                value={this.state.lastname}
                                onChangeText={lastname => this.setState({ lastname })}
                            />
                        </Item>
                        <Text>Birthdate</Text>
                        <Item rounded  >
                            <DatePicker
                                defaultDate={new Date(2000, 1, 1)}
                                minimumDate={new Date(1970, 1, 1)}
                                maximumDate={new Date(2012, 12, 31)}
                                locale={"en"}
                                // timeZoneOffsetInMinutes={undefined}
                                format="YYYY-MM-DD"
                                modalTransparent={false}
                                animationType={"fade"}
                                androidMode={"default"}
                                placeHolderText="Select date"
                                textStyle={{ color: "green" }}
                                placeHolderTextStyle={{ color: "#d3d3d3" }}
                                onDateChange={birthdate => this.setState({ birthdate })}
                                //onDateChange={this.state.bdate}
                                disabled={false}
                            />
                        </Item>
                        <Text>Username</Text>
                        <Item rounded  >
                            <Input placeholder='Enter Username'
                                value={this.state.username}
                                onChangeText={username => this.setState({ username })}
                            />
                        </Item>
                        <Text>Password</Text>
                        <Item rounded  >
                            <Input secureTextEntry={true}
                                placeholder='Enter password'
                                value={this.state.password}
                                onChangeText={password => this.setState({ password })}
                            />
                        </Item>
                        <View>
                            <Text>Gender:</Text>
                            <Picker
                                selectedValue={this.state.gender}
                                style={{ width: 150 }}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({ gender: itemValue })
                                    // onValueChange={gender => this.setState({ gender })
                                }>
                                <Picker.Item label="Male" value="male" />
                                <Picker.Item label="Female" value="female" />
                            </Picker>
                        </View>
                        <Button rounded danger
                            style={styles.button}
                            // onPress={() => Alert.alert('Right button pressed')}
                            onPress={() => { this._InsertRequest() }
                            }
                        ><Text style={{ color: 'white', fontSize: 20 }}>Register</Text></Button>
                    </View>
                    <View style={{ flex: 0.5 }} ></View>


                </View>
            </ScrollView>
        );
    }

    buttonListener() {
        const { firstname, lastname, gender, username, password,birthdate,admin } = this.state
        const bdate = moment(this.state.birthdate).format("YYYY-MM-DD")
        alert(firstname + ", " + lastname + ", " + gender + ", " + username + ", " + password + "," + bdate+ "," + admin)
    }

    _InsertRequest() {
        const bdate = moment(this.state.birthdate).format("YYYY-MM-DD")
        fetch('http://18.217.61.156/signup',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8'
                },
                body: JSON.stringify({
                    firstname: this.state.firstname,
                    lastname: this.state.lastname,
                    username: this.state.username,
                    password: this.state.password,
                    gender: this.state.gender,
                    birthdate: bdate,
                    admin: this.state.admin

                })
            })
            .then((response) => {
                response.json()
                    .then((data) => {
                        if (data.message === 'Success') {
                            alert("Successfully Registered! Please login")
                            this.props.navigation.navigate('LoginNav')

                        } else if (data.message === 'Username already exist'){
                            alert( "Username already exist")
                        } else {
                            alert( "Network")
                        }

                    })
            })
            .catch((error) => {
                alert("Opps I didn't again." + error)
            })

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        marginTop: 10, 
        marginLeft: 20, 
        marginRight: 20 
    },
      circle: {
          height: 20,
          width: 20,
          borderRadius: 10,
          borderWidth: 1,
          borderColor: '#ACACAC',
          alignItems: 'center',
      justifyContent: 'center',
      marginLeft: 20,
      marginTop: 5
      },
      checkedCircle: {
          width: 14,
          height: 14,
          borderRadius: 7,
          backgroundColor: '#794F9B',
    },  
    separator: {
      marginTop: 20,
      backgroundColor: '#eaeaea',
      height: 1,
    },
    button: {
        marginTop: 10, 
        marginLeft: 20, 
        marginRight: 20, 
        justifyContent: 'center'
    }
  });
