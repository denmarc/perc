import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity,
    Linking, Platform } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Hotline extends Component {


    render() {
        const { navigate } = this.props.navigation;
        
        return (
            <ScrollView>
                <View style={{ flex: 1, flexDirection: "row", alignItems: "stretch" ,alignSelf: "center"}}>
                    <TouchableOpacity onPress={() => navigate('FloorplanNav')}>
                            <Card style={styles.cardStyleMain}>
                                {/* <CardItem >
                                    <Image source={require('./assets/image/fracture.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 80,
                                        }} />
                                </CardItem> */}
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red"}}>FLOOR PLAN</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                </View>  

                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 20 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    {/* <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/phone.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View> */}
                    {/* <Card style={styles.cardStyle2}>
                        <CardItem >
                            <Body>
                                <Text style={{textAlign: "justify"}}>An Earthquake is the shaking of the surface of the Earth.
                                    Earthquakes can be extremely violent. Earthquakes are caused by
                                    tectonic movements in the Earth's crust. The main cause is when
                                    tectonic plates ride one over the other, causing orogeny
                                        (mountain building), and severe earthquakes.</Text>
                            </Body>
                        </CardItem>
                    </Card> */}
                    
                    <View style={{ flex: 2 ,alignSelf: "center"}}>
                                
                    <TouchableOpacity onPress={()=>{Linking.openURL('tel:02911');}} activeOpacity={0.7}>    
                        <Card style={styles.cardStyle}>    
                            <CardItem style={styles.cardStyle}>
                                <Left>
                                    <Body>
                                        <Text style={styles.cardText}>NATIONAL EMER HOTLINE</Text>
                                    </Body>
                                    <Thumbnail source={require('./assets/image/phone.png')} resizeMode='contain' />
                                </Left>
                            </CardItem> 
                        </Card>
                     </TouchableOpacity>
                     <TouchableOpacity onPress={()=>{Linking.openURL('tel:02161');}}  activeOpacity={0.7}> 
                        <Card style={styles.cardStyle}>    
                            <CardItem style={styles.cardStyle}>
                                <Left>
                                    <Body>
                                        <Text style={styles.cardText}>MARIKINA RESCUE PHONE</Text>
                                    </Body>
                                    <Thumbnail source={require('./assets/image/phone.png')} resizeMode='contain' />
                                </Left>
                            </CardItem> 
                        </Card>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{Linking.openURL('tel:09175842168');}}  activeOpacity={0.7}> 
                        <Card style={styles.cardStyle}>    
                            <CardItem style={styles.cardStyle}>
                                <Left>
                                    <Body>
                                        <Text style={styles.cardText}>MARIKINA RESCUE GLOBE</Text>
                                    </Body>
                                    <Thumbnail source={require('./assets/image/phone.png')} resizeMode='contain' />
                                </Left>
                            </CardItem> 
                        </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{Linking.openURL('tel:09285593341');}}  activeOpacity={0.7}> 
                        <Card style={styles.cardStyle}>    
                            <CardItem style={styles.cardStyle}>
                                <Left>
                                    <Body>
                                        <Text style={styles.cardText}>MARIKINA RESCUE SMART</Text>
                                    </Body>
                                    <Thumbnail source={require('./assets/image/phone.png')} resizeMode='contain' />
                                </Left>
                            </CardItem> 
                        </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{Linking.openURL('tel:0282739629');}}  activeOpacity={0.7}> 
                        <Card style={styles.cardStyle}>    
                            <CardItem style={styles.cardStyle}>
                                <Left>
                                    <Body>
                                        <Text style={styles.cardText}>MARIKINA BFP</Text>
                                    </Body>
                                    <Thumbnail source={require('./assets/image/phone.png')} resizeMode='contain' />
                                </Left>
                            </CardItem> 
                        </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{Linking.openURL('tel:0286461631');}}  activeOpacity={0.7}> 
                        <Card style={styles.cardStyle}>    
                            <CardItem style={styles.cardStyle}>
                                <Left>
                                    <Body>
                                        <Text style={styles.cardText}>MARIKINA PNP</Text>
                                    </Body>
                                    <Thumbnail source={require('./assets/image/phone.png')} resizeMode='contain' />
                                </Left>
                            </CardItem> 
                        </Card>
                        </TouchableOpacity>
                    </View>
                    

                </View>


            </ScrollView>

        )

    }
}

const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 30,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyleMain: {
        height: 50,
        width: 300,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 5,
    },
        cardText: {
            color: 'red'
        },        
    cardStyle: {
        height: 60,
        width: 300,
        marginBottom: 18,
        justifyContent: 'center',
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 9,
        // },
        // shadowOpacity: 0.50,
        // shadowRadius: 11.65,
        // elevation: 20,
    },
    cardStyle2: {
        height: 50,
        // width: 200,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});