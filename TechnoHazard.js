import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class TechnoHazard extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
            <View style={{ flex: 1, display: 'flex' }}>
                <View style={{ flex: 0.25 }} >
                    <Header style={{ backgroundColor: "gray", height: 30 }}>
                        {/* <Left>

                        </Left>
                        <Right>
                            <Body>
                                <Text style={{ color: "yellow" }}>Hello User!</Text>
                            </Body>
                        </Right> */}
                    </Header>
                </View>
                <View style={{ flex: 2, flexDirection: "column", alignItems: "center" }}>
                    <TouchableOpacity onPress={() => navigate('FireNav')}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/fire.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 70, width: 85,
                                    }} />
                            </CardItem>
                            <CardItem >

                                <Text style={{textAlign:"center", color:"red"}}>FIRE</Text>

                            </CardItem>
                        </Card>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigate('VehicularNav')}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/accident.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 70, width: 90,
                                    }} />
                            </CardItem>
                            <CardItem >

                                <Text style={{textAlign:"center", color:"red"}}>VEHICULAR ACCIDENT</Text>

                            </CardItem>
                        </Card>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => navigate('ExplosionNav')}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/explosion.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 75, width: 90,
                                    }} />
                            </CardItem>
                            <CardItem >

                                <Text style={{textAlign:"center", color:"red"}}>EXPLOSIONS</Text>

                            </CardItem>
                        </Card>
                    </TouchableOpacity>


                </View>
            </View>
            </ScrollView>
        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        marginLeft: 10,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    cardStyle: {
        height: 175,
        width: 175,
        marginBottom: 25,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});