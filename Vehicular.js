import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Vehicular extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
 
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/accident.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Causes:{"\n"}
                            1.	Distracted Driving{"\n"}
                            2.	Drunk Driving{"\n"}
                            3.	Speeding{"\n"}
                            4.	Reckless Driving{"\n"}
                            5.	Lost Control{"\n"}
                            6.	Lost brakes{"\n"}
                            7.	Mechanical Failure{"\n"}
                            8.	Using mobile device while driving{"\n"}
                            9.	Overtaking{"\n"}
                            10.	Beating the red light{"\n"}
                            </Text>
                        </Card>
                    </View>
                    <View style={{ flex: 3 }}>

                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/accident2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 370,
                                    alignSelf: "center"
                                }} />
                        </View>

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            What to do during: Minor Accidents{"\n"}
                            •	Safety first{"\n"}
                            •	Do not cause traffic{"\n"}
                            •	Interacting with the other driver{"\n"}
                            •	Insurance and repairs{"\n"}{"\n"}
                            What to do during: Major Accidents{"\n"}
                            •	Check if anyone is hurt{"\n"}
                            •	Make sure to take pictures of the accident{"\n"}
                            •	Move your vehicle off to the side of the road if possible{"\n"}
                            •	Let the investigator take command of the situation, 
                            and institute measures to prevent the accident from getting worse{"\n"}
                            •	Exchange information with the other driver.{"\n"}
                            •	Contact your insurance company and know what your policy covers.{"\n"}
                            •	 File an accident report with the police department.{"\n"}
                            </Text>
                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 5,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 250,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 400,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});