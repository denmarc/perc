import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Dengue extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/dengue.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Dengue (pronounced DENgee) fever is a painful, debilitating mosquito-borne disease
                             caused by any one of four closely related dengue viruses. These viruses are related 
                             to the viruses that cause West Nile infection and yellow fever.
                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/dengues.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

            

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            Symptoms of Dengue Fever {"\n"} {"\n"}
                            Symptoms, which usually begin four to six days after infection and last for up to 10 days,
                             may include {"\n"}
                            •	Sudden, high fever {"\n"}
                            •	Severe headaches {"\n"}
                            •	Pain behind the eyes {"\n"}
                            •	Severe joint and muscle pain {"\n"}
                            •	Fatigue {"\n"}
                            •	Nausea {"\n"}
                            •	Vomiting {"\n"}
                            •	Skin rash, which appears two to five days after the onset of fever {"\n"}
                            •	Mild bleeding (such a nose bleed, bleeding gums, or easy bruising) {"\n"} {"\n"}
                            Treatment for Dengue Fever {"\n"}
                            There is no specific medicine to treat dengue infection. If you think you may have dengue 
                            fever, you should use pain relievers with acetaminophen and avoid medicines with aspirin,
                             which could worsen bleeding. You should also rest, drink plenty of fluids, and see your 
                             doctor. If you start to feel worse in the first 24 hours after your fever goes down, you 
                             should get to a hospital immediately to be checked for complications. {"\n"} {"\n"}
                            Preventing Dengue Fever {"\n"}
                            The best way to prevent the disease is to prevent bites by infected mosquitoes, 
                            particularly if you are living in or traveling to a tropical area. This involves 
                            protecting yourself and making efforts to keep the mosquito population down. In 2019,
                             the FDA approved a vaccine called Dengvaxia to help prevent the disease from occurring
                              in adolescents aged 9 to 16 who have already been infected by dengue. But, there 
                              currently is no vaccine to prevent the general population from contracting it. {"\n"}
                           

                            </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 140,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 880,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});