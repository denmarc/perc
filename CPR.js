import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class CPR extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/cpr.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            CPR is a medical technique for reviving someone whose heart has stopped 
                            beating by pressing on their chest and breathing into their mouth. 
                            CPR is an abbreviation for cardiopulmonary resuscitation.
                            </Text>
                        </Card>
                    </View>
                    <View style={{ flex: 3 }}>

                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/cpr2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 370,
                                    alignSelf: "center"
                                }} />
                        </View>

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            Compressions: Restore blood circulation{"\n"}
                            1.	Put the person on his or her back on a firm surface.{"\n"}
                            2.	Kneel next to the person's neck and shoulders.{"\n"}
                            3.	Place the heel of one hand over the center of the person's chest, 
                            between the nipples. Place your other hand on top of the first hand. 
                            Keep your elbows straight and position your shoulders directly above your hands.{"\n"}
                            4.	Use your upper body weight (not just your arms) as you push straight 
                            down on (compress) the chest at least 2 inches (approximately 5 centimeters)
                             but not greater than 2.4 inches (approximately 6 centimeters).
                              Push hard at a rate of 100 to 120 compressions a minute.{"\n"}
                            5.	If you haven't been trained in CPR, continue chest compressions until 
                            there are signs of movement or until emergency medical personnel take over.
                             If you have been trained in CPR, go on to opening the airway and rescue breathing.{"\n"}{"\n"}
                            Airway: Open the airway{"\n"}
                            •	If you're trained in CPR and you've performed 30 chest compressions, 
                            open the person's airway using the head-tilt, chin-lift maneuver. {"\n"}
                            Put your palm on the person's forehead and gently tilt the head back. {"\n"}
                            Then with the other hand, gently lift the chin forward to open the airway.{"\n"}{"\n"}

                            Breathing: Breathe for the person{"\n"}
                            Rescue breathing can be mouth-to-mouth breathing or mouth-to-nose breathing
                             if the mouth is seriously injured or can't be opened.
                            1.	With the airway open (using the head-tilt, chin-lift maneuver), 
                            pinch the nostrils shut for mouth-to-mouth breathing and cover the person's mouth with yours,
                             making a seal.{"\n"}
                            2.	Prepare to give two rescue breaths. Give the first rescue breath — lasting one second — 
                            and watch to see if the chest rises. If it does rise, give the second breath. 
                            If the chest doesn't rise, repeat the head-tilt, chin-lift maneuver and then 
                            give the second breath. Thirty chest compressions followed by two rescue breaths
                             is considered one cycle. Be careful not to provide too many breaths or to breathe 
                             with too much force.{"\n"}
                            3.	Resume chest compressions to restore circulation.{"\n"}
                            4.	As soon as an automated external defibrillator (AED) is available, apply it 
                            and follow the prompts. Administer one shock, then resume CPR — starting with chest 
                            compressions — for two more minutes before administering a second shock. If you're not 
                            trained to use an AED, an emergency medical operator may be able to guide you in its use.
                             If an AED isn't available, go to step 5 below.{"\n"}
                            5.	Continue CPR until there are signs of movement or emergency medical personnel take over.{"\n"}

                            </Text>
                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 5,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 120,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1300,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});