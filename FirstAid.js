import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class FirstAid extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
            <View style={{ flex: 1, display: 'flex' }}>
                <View style={{ flex: 0.40 }} >
                    <Header style={{ backgroundColor: "gray", height: 30 }}>
                        {/* <Left>

                        </Left>
                        <Right>
                            <Body>
                                <Text style={{ color: "yellow" }}>PERC</Text>
                            </Body>
                        </Right> */}
                    </Header>
                </View>
                <View style={{ flex: 2.5, flexDirection: "column", alignItems: "center" }}>
                    <View style={{ flex: 1, flexDirection: "row", alignItems: "stretch" }}>
                        <TouchableOpacity onPress={() => navigate('BurnsNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/burn.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 80,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red"}}>BURNS</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('CPRNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/cpr.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 85,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red"}}>CPR</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row", alignItems: "stretch" }}>
                        <TouchableOpacity onPress={() => navigate('SprainNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/sprain.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 95,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red",fontSize:13}} >SPRAIN / STRAIN</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('NosebleedNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/nosebleed.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 80,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red"}}>NOSEBLEED</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row", alignItems: "stretch" }}>
                        <TouchableOpacity onPress={() => navigate('CutsNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/cut.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 95,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red",fontSize:13}} >CUTS AND SCRAPES</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('FracturesNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/fracture.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 80,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red"}}>FRACTURES</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row", alignItems: "stretch" }}>
                        <TouchableOpacity onPress={() => navigate('RabiesNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/rabies.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 95,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red",fontSize:13}} >RABIES</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('TetanusNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/tetanus.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 80,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red"}}>TETANUS</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row", alignItems: "stretch" }}>
                        <TouchableOpacity onPress={() => navigate('SplinterNav')}>
                            <Card style={styles.cardStyle}>
                                <CardItem >
                                    <Image source={require('./assets/image/splint.png')}
                                        style={{
                                            resizeMode: "stretch",
                                            height: 80, width: 95,
                                        }} />
                                </CardItem>
                                <CardItem >

                                    <Text style={{textAlign:"center", color:"red",fontSize:13}} >SPLINTER</Text>

                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        marginLeft: 10,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    cardStyle: {
        height: 175,
        width: 175,
        marginLeft: 15,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});