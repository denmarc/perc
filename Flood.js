import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Flood extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
  
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/flood.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            A flood is an overflow of water that submerges land that is usually dry.
                             In the sense of "flowing water", the word may also be applied to the inflow 
                             of the tide. Flooding may occur as an overflow of water from water bodies, 
                             such as a river, lake, or ocean, resulting in some of that water escaping 
                             its usual boundaries or it may occur due to an accumulation of rainwater on 
                             saturated ground in an area flood.
                            </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 3 }}>
                        <View style={styles.imageStyle2}>
                            <Image source={require('./assets/image/flood2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                                What to do before a flood? (When flooding is forecast).{"\n"}
                                •	Monitor your surroundings.{"\n"}
                                •	Monitor local television and radio stations, or go to www.accuweather.com {"\n"}{"\n"}

                                Note: If a flash flood warning is issued for your area:{"\n"} 
                                Climb to safety immediately.{"\n"}
                                •	Flash floods develop quickly. Do not wait until you see rising water.{"\n"}
                                •	Get out of low areas subject to flooding.{"\n"}
                                •	If driving, do not drive through flooded roadways!{"\n"}{"\n"}

                                Assemble disaster supplies:{"\n"}
                                •	Drinking water – Fill clean containers.{"\n"}
                                •	Food that requires no refrigeration or cooking.{"\n"}
                                •	Cash.{"\n"}
                                •	Medications and first aid supplies.{"\n"}
                                •	Clothing, toiletries.{"\n"}
                                •	Battery-powered radio.{"\n"}
                                •	Flashlights.{"\n"}
                                •	Extra batteries.{"\n"}
                                •	Important documents: insurance papers, medical records, bank account numbers.{"\n"}{"\n"}
                                What to do during a Flood?{"\n"}
                                •	Get out of low areas that may be subject to flooding.{"\n"}
                                •	Avoid already-flooded areas and do not attempt to cross flowing water.{"\n"}
                                •	Stay away from power lines and electrical wires.
                                Evacuate immediately, if you think you are at risk or are advised to do so!{"\n"}
                                •	Act quickly. Save yourself, not your belongings.{"\n"}
                                •	Move to a safe area before access is cut off by rising water.{"\n"}
                                •	Families should use only one vehicle to avoid getting separated 
                                and reduce traffic jams.{"\n"}
                                •	Shut off water, gas, and electrical services before leaving.{"\n"}
                                •	Secure your home: lock all doors and windows.{"\n"}
                                •	If directed to a specific location, go there.{"\n"}{"\n"}

                                What to do after a Flood?{"\n"}{"\n"}

                                •  Monitor local television and radio stations.{"\n"}
                                •  Do not return to flooded areas until authorities indicate it is safe to do so.{"\n"}
                                •  Do not visit disaster areas following a flood. Your presence may hamper 
                                urgent emergency response and rescue operations.{"\n"}

                            </Text>
                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 30,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 200,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1100,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});