import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Earthquake extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/earthquake.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                                An Earthquake is the shaking of the surface of the Earth.
                                            Earthquakes can be extremely violent. Earthquakes are caused by
                                            tectonic movements in the Earth's crust. The main cause is when
                                            tectonic plates ride one over the other, causing orogeny
                                            (mountain building), and severe earthquakes.
                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/earthquake2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/duringearthquake.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

                        <View style={styles.imageStyle2}>
                            <Image source={require('./assets/image/earthquakecover.jpg')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

                        <View style={styles.imageStyle2}>
                            <Image source={require('./assets/image/afterearthquake.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

                        <View style={styles.imageStyle2}>
                            <Image source={require('./assets/image/earthquakepeople.jpg')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>


                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            https://www.youtube.com/watch?v=gZ9T2e7GFxg&t=209s{"\n"}{"\n"}
                            What to do before an Earthquake?{"\n"}{"\n"}
                                •	Make sure you have a fire extinguisher, first aid kit, 
                                a battery-powered radio, a flashlight, and extra batteries at home.{"\n"}
                                •	Learn first aid.{"\n"}
                                •	Learn how to turn off the gas, water, and electricity.{"\n"}
                                •	Make up a plan of where to meet your family after an earthquake.{"\n"}
                                •	Don't leave heavy objects on shelves (they'll fall during a quake).{"\n"}
                                •	Learn the earthquake plan at your school or workplace.{"\n"}{"\n"}
                                What to do during an Earthquake?{"\n"}
                                •	Duck, Cover and Hold.{"\n"}
                                •	Stay calm! If you're indoors, stay inside. If you're outside, stay outside.
                                •	If you're indoors, stand against a wall near the center of the building, 
                                stand in a doorway, or crawl under heavy furniture (a desk or table). 
                                Stay away from windows and outside doors.{"\n"}
                                •	If you're outdoors, stay in the open away from power lines or anything 
                                that might fall. Stay away from buildings (stuff might fall off the building 
                                or the building could fall on you).{"\n"}
                                •	If you're in a car, stop the car and stay inside the car until the earthquake
                                 stops.{"\n"}
                                •	Don't use elevators (they'll probably get stuck anyway).{"\n"}{"\n"}
                                What to do after an Earthquake?{"\n"}
                                •	Check yourself and others for injuries. Provide first aid for anyone
                                 who needs it.{"\n"}
                                •	Turn on the radio.{"\n"}
                                •	Stay out of damaged buildings.{"\n"}
                                •	Be careful around broken glass and debris. Wear boots or sturdy shoes 
                                to keep from cutting your feet.{"\n"}
                                •	Stay away from beaches. Tsunamis sometimes hit after the ground has 
                                stopped shaking.{"\n"}
                                •	Stay away from damaged areas.{"\n"}
                                •	If you're at school or work, follow the emergency plan or the instructions 
                                of the person in charge.{"\n"}
                                •	Expect aftershocks.{"\n"}



                            </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 150,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1050,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});