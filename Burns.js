import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Burns extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
        
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/burn.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Burns can result from contact with heat, fire, radiation, sunlight, chemical 
                            or boiling water.{"\n"}{"\n"}
                            Burns are one of the most common household injuries, especially among children. 
                            The term “burn” means more than the burning sensation associated with this injury. 
                            Burns are characterized by severe skin damage that causes the affected skin cells
                             to die.
                            </Text>
                        </Card>
                    </View>
                    <View style={{ flex: 3 }}>

                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/burn2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 370,
                                    alignSelf: "center"
                                }} />
                        </View>

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            1st degree burn{"\n"}
                            First-degree burns cause minimal skin damage. They are also called “superficial burns”
                            use they affect the outermost layer of skin. Signs of a first-degree burn include:{"\n"}
                            •	redness{"\n"}
                            •	minor inflammation, or swelling{"\n"}
                            •	pain{"\n"}
                            •	dry, peeling skin occurs as the burn heals{"\n"}{"\n"}
                            First-degree burns are usually treated with home care.
                             Healing time may be quicker the sooner you treat the burn. Treatments for a 
                             first-degree burn include:{"\n"}
                            •	soaking the wound in cool water for five minutes or longer{"\n"}
                            •	taking acetaminophen or ibuprofen for pain relief{"\n"}
                            •	applying lidocaine (an anesthetic) with aloe vera gel or cream to soothe the skin{"\n"}
                            •	using an antibiotic ointment and loose gauze to protect the affected area{"\n"}{"\n"}
                            2nd degree burn{"\n"}
                            Second-degree burns are more serious because the damage extends beyond the top 
                            layer of skin. This type burn causes the skin to blister and become extremely red and sore.
                            Some blisters pop open, giving the burn a wet or weeping appearance. Over time, thick, soft, 
                            scab-like tissue called fibrinous exudate may develop over the wound.
                            As with first-degree burns, avoid cotton balls and questionable home remedies. Treatments for
                             a mild second-degree burn generally include:{"\n"}
                            •	running the skin under cool water for 15 minutes or longer{"\n"}
                            •	taking over-the-counter pain medication (acetaminophen or ibuprofen){"\n"}
                            •	applying antibiotic cream to blisters{"\n"}{"\n"}
                            3rd degree burns{"\n"}
                            third-degree burns are the most severe. They cause the most damage, extending through every layer of skin.
                            Never attempt to self-treat a third-degree burn. Call emergency hotline immediately While you’re 
                            waiting for medical treatment, raise the injury above your heart. Don’t get undressed, but make 
                            sure no clothing is stuck to the burn.{"\n"}
                            A chemical burn is irritation and destruction of human tissue caused by exposure to a chemical, 
                            usually by direct contact with the chemical or its fumes. Chemical burns can occur in the home, 
                            at work or school, or as a result of accident or assault.{"\n"}{"\n"}
                            What to do{"\n"}
                            •	Remove yourself or the injured person from the accident or exposure area. Take appropriate care 
                            not to cause further injury to the patient.{"\n"}
                            •	Remove any contaminated clothing.{"\n"}
                            •	Wash the injured area to dilute or remove the substance, using large volumes of water. Wash for
                             at least 20 minutes, taking care not to allow runoff to contact unaffected parts of anyone's body. 
                             Gently brush away any solid materials, again avoiding unaffected body surfaces.{"\n"}
                            •	Especially wash away any chemical in the eyes. Sometimes the best way to get large amounts of water
                             to the eyes is to take a shower. If there is an eye wash station nearby (usually fond at work sites),
                              follow the simple instructions to rinse out the eyes.{"\n"}
                            </Text>
                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 5,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 200,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1500,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});