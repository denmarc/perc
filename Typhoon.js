import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Typhoon extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
  
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/typhoon.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            Tropical cyclone, also called typhoon is an intense circular storm that 
                            originates over warm tropical oceans and is characterized by low atmospheric
                             pressure, high winds, and heavy rain.
                            </Text>
                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/typhoon2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                                Before the Typhoon:{"\n"}
                                	Store an adequate supply of food and clean water.{"\n"}
                                	Prepare foods that need not be cooked.{"\n"}
                                	Keep flashlights, candles and battery-powered radios within easy reach.{"\n"}
                                	Examine your house and repair its unstable parts.{"\n"}
                                	Always keep yourself updated with the latest weather report.{"\n"}
                                	Secure domesticated animals in a safe place.{"\n"}
                                	Should you need to evacuate, bring clothes, first aid kit, 
                                candles/flashlight, battery-powered radio, food, etc. {"\n"}{"\n"}
                                
                                During the Typhoon:{"\n"}
                                	Stay inside the house.{"\n"}
                                	Always keep yourself updated with the latest weather report.{"\n"}
                                	If safe drinking water is not available, boil water for at least 20 minutes.
                                 Place it in a container with cover.{"\n"}
                                	Keep an eye on lighted candles or gas lamps.{"\n"}
                                	Do not wade through floodwaters to avoid being electrocuted and 
                                contracting diseases.{"\n"}
                                	If there is a need to move to an evacuation center, follow these reminders.{"\n"}
                                	Evacuate calmly.{"\n"}
                                	Close the windows and turn off the main power switch.{"\n"}
                                	Put important appliances and belongings in a high ground.{"\n"}
                                	Avoid the way leading to the river.{"\n"}{"\n"}
                                
                                After the Typhoon:{"\n"}
                                	If your house was destroyed, make sure that it is already safe and 
                                stable when you enter.{"\n"}
                                	Beware of dangerous animals such as snakes that may have entered your house.{"\n"}
                                	Watch out for live wires or outlet immersed in water.{"\n"}
                                	Report damaged electrical cables and fallen electric posts to the authorities.{"\n"}
                                	Do not let water accumulate in tires, cans or pots to avoid creating a 
                                favorable condition for mosquito breeding.{"\n"}

                            </Text>
                        </Card>
                    </View>
                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 30,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 120,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 900,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});