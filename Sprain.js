import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Sprain extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                           
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/sprain.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            A sprain is a stretching or tearing of ligaments — the tough bands of fibrous 
                            tissue that connect two bones together in your joints. The most common location
                             for a sprain is in your ankle. The difference between a sprain and a strain is
                              that a sprain injures the bands of tissue that connect two bones together, 
                              while a strain involves an injury to a muscle or to the band of tissue that 
                              attaches a muscle to a bone.
                            </Text>
                        </Card>
                    </View>
                    <View style={{ flex: 3 }}>

                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/sprains2.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 370,
                                    alignSelf: "center"
                                }} />
                        </View>

                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            Causes{"\n"}
                                A sprain occurs when you overextend or tear a ligament while severely 
                                stressing a joint. Sprains often occur in the following circumstances:{"\n"}
                                •	Ankle — Walking or exercising on an uneven surface, landing awkwardly 
                                from a jump{"\n"}
                                •	Knee — Pivoting during an athletic activity{"\n"}
                                •	Wrist — Landing on an outstretched hand during a fall{"\n"}
                                •	Thumb — Skiing injury or overextension when playing racquet sports, 
                                such as tennis{"\n"}{"\n"}
                                What to do {"\n"}
                                Use the RICE approach. Rest, ice, compression and elevation{"\n"}
                                Step 1. Rest{"\n"}
                                Pain is your body’s signal that something is wrong. As soon as you’re hurt, 
                                stop your activity, and rest as much as possible for the first 2 days.{"\n"}{"\n"}
                                Step 2: Ice{"\n"}
                                Ice is a tried-and-true tool for reducing pain and swelling. Apply an ice pack
                                 (covered with a light, absorbent towel to help prevent frostbite) for 
                                 15-20 minutes every two to three hours during the first 24 to 48 hours 
                                 after your injury. {"\n"}{"\n"}
                                Step 3: Compression{"\n"}
                                This means wrapping the injured area to prevent swelling. Wrap the affected 
                                area with an elastic medical bandage (like an ACE bandage). You want it to be 
                                snug but not too tight -- if it’s too tight, it’ll interrupt blood flow.
                                 If the skin below the wrap turns blue or feels cold, numb, or tingly,
                                  loosen the bandage. If these symptoms don’t disappear right away, 
                                  seek immediate medical help.{"\n"}{"\n"}

                                Step 4: Elevation{"\n"}
                                This means raising the sore body part above the level of your heart. 
                                Doing so reduces pain, throbbing, and swelling. It’s not as tricky to do as you
                                 might think. For example, if you have an ankle sprain, you can prop your 
                                 leg up on pillows while sitting on the sofa. The CDC recommends you keep
                                  the injured area raised whenever possible, even when you’re not icing it.{"\n"}
                                </Text>
                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 5,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 220,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1000,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});