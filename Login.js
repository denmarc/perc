
import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput } from 'react-native';
import {
    Container, Header, Title, Content,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input
} from 'native-base';


export default class Login extends Component {
    state = {
        username: '',
        password: ''}  
    render() {
         const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, display: 'flex' }}>
                <View style={{ flex: 0.3, backgroundColor:"white" }}>
                    {/* <View style={{
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor:"red"
                    }}>
                    </View> */}
                </View>
                <View style={{
                    backgroundColor: 'white', flex: 2, 
                    flexDirection: 'column',
                    justifyContent: "center",
                }}>
                    <View style={{ flex: 0.6 }}>
                        <Image
                            resizeMode='stretch'
                            style={{
                                width: 210,
                                height: 120,
                                alignSelf: 'center',

                            }}
                            source={require('./assets/image/logo.png')}
                        />
                    </View>
                    <View style={{
                        backgroundColor: 'white',
                        flex: 1.5
                    }}>

                        <Item rounded style={{ marginTop: 10, marginLeft: 20, marginRight: 20 }}>
                            <Image source={require('./assets/image/person.png')} style={styles.ImageStyle2} />
                            <Input 
                                placeholder='Please input username'
                                value={this.state.username}
                                onChangeText={username => this.setState({ username })}
                            />
                        </Item>
                        <Item rounded style={{ marginTop: 10, marginLeft: 20, marginRight: 20 }}>
                        <Image source={require('./assets/image/key.png')} style={styles.ImageStyle2} />
                            <Input
                                secureTextEntry={true} 
                                placeholder='Please input Password'
                                value={this.state.password}
                                onChangeText={password => this.setState({ password })}
                            />
                        </Item>
                        <View style={{ flex: 0.1 }} >
                            {/* <Text style={{

                                textAlign: 'right',
                                marginTop: 10,
                                marginRight: 20
                            }}
                                // onPress={() => navigate('OnBoardingSample')}
                                >Forgot Password?
                             </Text> */}
                        </View>
                        
                       
                        <Button rounded danger
                            style={{ marginTop: 10, marginLeft: 20, marginRight: 20,justifyContent:'center' }}
                             onPress={() => alert('Login pressed')}
                            onPress={() => {this._postRequest()}}
                            //onPress={() => navigate('HomeScreenNav')}
                            // }
                        ><Text style={{ color: 'white', fontSize: 20}}>Login</Text></Button>
                        
                        <View style={{
                            flex: 0.4, flexDirection: 'column',
                            justifyContent: "center",
                             alignItems: "center",
                            
                        }} >
                            <View style={{
                                flex: 0.5, marginLeft: 20,justifyContent: "center",
                            }}>
                                <Text
                                >Don't have an account yet?
                                </Text>
                            </View>
                            <View style={{
                                flex: 0.5
                            }}>
                                <Text
                                    style={{ color: 'red' }}
                                    onPress={() => navigate('RegisterNav')}
                                    // onPress={() => navigate('IntelliMapSample')}
                                    >
                                    Sign up
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
                {/* <View style={{ flex: 0.5 }}></View> */}
            </View>



        );
    }

    _getRequest() {
        fetch('http://18.217.61.156/users',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            })
            .then((response) => {
                response.json()
                    .then((data) => {
                        alert(JSON.stringify(data))
                        this.props.navigation.navigate('HomeScreenNav')
                    })
            })
            .catch((error) => {
                alert("Opps I didn't again." + error)
            })
    }

    _postRequest() {
        fetch('http://18.217.61.156/login',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=UTF-8'
                },
                body: JSON.stringify({
                    username: this.state.username,
                    password: this.state.password,

                })
            })
            .then((response) => {
                response.json()
                    .then((data) => {
                        if (data.message ==='User found') {
                            alert("Welcome! " + this.state.username)
                            this.props.navigation.navigate('HomeScreenNav')

                        } else {
                            alert("Username and password are incorrect")
                        }

                    })
            })
            .catch((error) => {
                alert("Opps I didn't again." + error)
            })

    }
}



const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        marginLeft:10,
        resizeMode : 'stretch',
        alignItems: 'center'
    }


});

