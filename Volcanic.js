import React, { Fragment, Component } from 'react';
import { StyleSheet, ScrollView, Image, StatusBar, View, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import {
    Container, Header, Title, Content, Card, CardItem,
    Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Item, Input, Thumbnail
} from 'native-base';

export default class Volcanic extends Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={{ flex: 1, display: 'flex' }}>
                    <View style={{ flex: 0.25 }} >
                        <Header style={{ backgroundColor: "gray", height: 30 }}>
                            {/* <Left>

                            </Left>
                            <Right>
                                <Body>
                                    <Text style={{ color: "yellow" }}>Hello User!</Text>
                                </Body>
                            </Right> */}
                        </Header>
                    </View>

                    <View style={{ flex: 0.3, alignItems: "center" }}>
                        <Card style={styles.cardStyle}>
                            <CardItem >
                                <Image source={require('./assets/image/volcanic.png')}
                                    style={{
                                        resizeMode: "stretch",
                                        height: 40, width: 45,
                                    }} />
                            </CardItem>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle2}>
                            <Text style={{ marginBottom: 10 }}>
                            A volcanic eruption occurs when hot materials from the Earth's interior are thrown out
                             of A volcano is an opening in the Earth’s crust that allows molten rock, gases, 
                             and debris to escape to the surface. Alaska, Hawaii, California, and Oregon have 
                             the most active volcanoes, but other states and territories have active volcanoes, too. 
                             A volcanic eruption may involve lava and other debris that can flow up to 100 mph, 
                             destroying everything in their path. Volcanic ash can travel 100s of miles and cause
                              severe health problems.
                                         </Text>

                        </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                        <View style={styles.imageStyle2}>

                            <Image source={require('./assets/image/volcanics.png')}
                                style={{
                                    resizeMode: "stretch",
                                    height: 220, width: 350,
                                    alignSelf: "center"
                                }} />
                        </View>

                      


                    </View>
                    <View style={{ flex: 2 }}>
                        <Card style={styles.cardStyle3}>
                            <Text style={{ marginBottom: 10 }}>
                            IF YOU ARE UNDER A VOLCANO WARNING:{"\n"}{"\n"}
                        •	Listen for emergency information and alerts.{"\n"}
                        •	Follow evacuation or shelter orders. If advised to evacuate, then do so early.{"\n"}
                        •	Avoid areas downstream of the eruption.{"\n"}
                        •	Protect yourself from falling ash.{"\n"}
                        •	Do not drive in heavy ash fall.{"\n"}{"\n"}
                        Before{"\n"}
                        •	Know your area’s risk from volcanic eruption.{"\n"}
                        •	Ask local emergency management for evacuation and shelter plans, and for potential
                         means of protection from ash.{"\n"}
                        •	Learn about community warning systems by signing up for a free service called the 
                        Volcano Notification Service (VNS) that sends notifications about volcanic activity.{"\n"}
                        •	Get necessary supplies in advance in case you have to evacuate immediately, or if
                         services are cut off. Keep in mind each person’s specific needs, including medication.
                          Do not forget the needs of pets.{"\n"}
                        •	Consult your doctor if you have existing respiratory difficulties.{"\n"}
                        •	Practice a communication and evacuation plan with everyone in your family.{"\n"}
                        •	Have a shelter-in-place plan if your biggest risk is from ash.{"\n"}
                        •	Keep important documents in a safe place. Create password-protected digital copies.{"\n"}
                        •	Find out what your homeowner’s insurance policy will cover when a volcano erupts.{"\n"}{"\n"}
                        DURING{"\n"}
                        •	Listen to alerts. The Volcano Notification Service provides up-to-date information about
                         eruptions.{"\n"}
                        •	Follow evacuation orders from local authorities. Evacuate early.{"\n"}
                        •	Avoid areas downwind, and river valleys downstream, of the volcano. Rubble and ash will be
                        carried by wind and gravity.{"\n"}
                        •	Take temporary shelter from volcanic ash where you are if you have enough supplies. Cover 
                        ventilation openings and seal doors and windows.{"\n"}
                        •	If outside, protect yourself from falling ash that can irritate skin and injure breathing 
                        passages, eyes, and open wounds. Use a well-fitting, certified face mask such as an N95. 
                        The Centers for Disease Control and Prevention (CDC) has a list of certified masks and the 
                        maker’s instructions on how to use the masks.{"\n"}
                        •	Avoid driving in heavy ash fall.{"\n"}{"\n"}
                        AFTER{"\n"}
                        Listen to authorities to find out when it is safe to return after an eruption.{"\n"}
                        •	Send text messages or use social media to reach out to family and friends. Phone systems 
                        are often busy after a disaster. Only make emergency calls.{"\n"}
                        •	Avoid driving in heavy ash. Driving will stir up volcanic ash that can clog engines and 
                        stall vehicles.{"\n"}
                        •	If you have any breathing problems, avoid contact with ash. Stay indoors until authorities
                         say it is safe to go outside.{"\n"}
                        •	Do not get on your roof to remove ash unless you have guidance or training. If you have to
                         remove ash, then be very careful as ash makes surfaces slippery. Be careful not to contribute 
                         additional weight to an overloaded roof.{"\n"}
                           </Text>


                        </Card>
                    </View>

                </View>
            </ScrollView>

        )

    }
}
const styles = StyleSheet.create({
    imageStyle: {
        marginTop: 32,
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
    },
    ImageStyle2: {
        padding: 10,
        // margin: 5,
        height: 400,
        width: 500,
        marginLeft: 10,
        marginBottom: 20,
        resizeMode: 'stretch',
        alignItems: 'center',
        flex: 1
    },
    cardStyle: {
        height: 50,
        width: 60,
        marginBottom: 18,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle2: {
        height: 250,
        // width: 200,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    },
    cardStyle3: {
        height: 1500,
        padding: 10,
        // width: 200,
        marginBottom: 10,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 11.65,
        elevation: 20,
    }


});